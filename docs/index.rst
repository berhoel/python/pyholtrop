.. include:: ../README.rst

API documentation
=================

.. currentmodule:: berhoel

.. autosummary::
   :nosignatures:

   holtrop

.. rubric:: References
.. bibliography::

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   _tmp/modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
