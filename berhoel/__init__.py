"""Initialization for berhoel python namespace packages."""

from __future__ import annotations

from pkgutil import extend_path

__date__ = "2024/07/25 21:22:35 hoel"
__author__ = "Berthold Höllmann"
__copyright__ = "Copyright © 2017 by Berthold Höllmann"
__credits__ = ["Berthold Höllmann"]
__maintainer__ = "Berthold Höllmann"
__email__ = "berhoel@gmail.com"


__path__ = extend_path(__path__, __name__)
