from __future__ import annotations

import tkinter

import Pmw
from astropy.units import Quantity  # type: ignore

from . import ship

_open = open


class ReadShip:
    __keys: list[str] = []

    def __init__(self, file=None):
        if file is None:
            self.ship = ship.Ship()
        else:
            self.ship = self.open(file)

    def open(self, file=None):
        out = ship.Ship()
        try:
            f = _open(file, "r")
        except OSError:
            return out

        lines = f.readlines()
        for line in lines:
            line = line.strip().split("#")[0].strip()
            if len(line) > 0:
                res = line.split("=")
                key, value = res[0].strip(), Quantity(res[1].strip())
                out[key] = value
                self.__keys.append(key)
        return out

    def edit(self, master):
        self.dialog = Pmw.Dialog(
            master,
            buttons=("Save", "Cancel"),
            defaultbutton="Cancel",
            title="Edit ship's properties",
        )
        #       master.withdraw()
        new_master = self.dialog.interior()
        self.dialog.grid()
        self.selbox = Pmw.ComboBox(
            new_master,
            label_text="select entity:",
            labelpos="w",
            selectioncommand=self.execute,
            scrolledlist_items=self.__keys,
        )
        self.selbox.grid()
        firstKey = self.__keys[0]
        self.selbox.selectitem(firstKey)

    #       master.activate()

    def execute(self, entity):
        text = "entity " + entity
        print(text)


def open(name):
    return ReadShip().open(name)


def main():
    a = ReadShip().open("tst.shp")
    print(a)
    a = ReadShip("tst.shp")

    root = tkinter.Tk()
    root = Pmw.initialise(root)
    a.edit(root)
    root.mainloop()
    print(a)


if __name__ == "__main__":
    main()
