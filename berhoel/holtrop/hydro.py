"""Hydrostatic and hydrodynamic parameters."""

from math import log10

from astropy import units as u  # type: ignore

__date__ = "2024/08/01 21:23:15 hoel"
__author__ = "Berthold Höllmann"
__copyright__ = "Copyright © 2019 by Berthold Höllmann"
__credits__ = ["Berthold Höllmann"]
__maintainer__ = "Berthold Höllmann"
__email__ = "berhoel@gmail.com"


g: u = 9.81 * u.m / u.s**2
nue: u = 1.1883e-6 * u.m**2 / u.s
"For seewater, salinity 3.5%,"
rho: u = 1.025 * 1000 * u.kg / u.m**3
"15°C, according to :cite:`schneekluthHydromechanikSchiffsentwurf1988`, p. 351"


def nue_calc(salin: u, temp: u):
    """Calculates the kinematic viscisity of water, returns value in m^2/s

    salin = salinity of the water
    temp  = temperature of the water  in degrees centigrade
    """
    return 1.0e-6 * (0.014 * salin + (0.000645 * temp - 0.0503) * temp + 1.75)


def F_n(speed: u, L: u):
    """Froude number, according to
    :cite:`schneekluthHydromechanikSchiffsentwurf1988`, p. 323
    """
    return (speed / pow(L * g, 0.5)).value


def R_n(speed: u, L: u):
    """Reynolds number, according to
    :cite:`schneekluthHydromechanikSchiffsentwurf1988`, p. 323
    """
    return speed * L / nue


def C_F(speed: u, L: u):
    """Coefficient of frictional resistance according to the ITTC-1957 formula,
    :cite:`holtropStatisticalPowerPrediction1978`, p253
    """
    return 0.075 / pow((log10(R_n(speed, L).value) - 2.0), 2.0)
