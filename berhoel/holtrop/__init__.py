# -*- coding: utf-8 -*-
"""Ship resistance acc. to Holtrop."""
from __future__ import annotations

__date__ = "2024/08/01 22:11:25 hoel"
__author__ = "Berthold Höllmann"
__copyright__ = "Copyright © 2020 by Berthold Höllmann"
__credits__ = ["Berthold Höllmann"]
__maintainer__ = "Berthold Höllmann"
__email__ = "berhoel@gmail.com"
